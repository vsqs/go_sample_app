package updater

import (
	"context"
	"gitlab.com/vsqs/go_sample_app/pkg/domain"
)

type ListRequest struct {
}

type ListResponse struct {
	Charts []domain.Chart
}

func (l ListResponse) ToUpdateRequest() UpdateRequest {
	return UpdateRequest{
		Charts: l.Charts,
	}
}

type UpdateRequest struct {
	Charts []domain.Chart
}

type UpdateResponse struct {
}

type Updater interface {
	List(ctx context.Context, request ListRequest) (ListResponse, error)
	Update(ctx context.Context, request UpdateRequest) (UpdateResponse, error)
}
