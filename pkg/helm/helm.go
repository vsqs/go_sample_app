package helm

import (
	"context"
	"gitlab.com/vsqs/go_sample_app/pkg/updater"
)

type helmUpdater struct {
}

func (h *helmUpdater) List(ctx context.Context, request updater.ListRequest) (updater.ListResponse, error) {
	//TODO implement me
	panic("implement me")
}

func (h *helmUpdater) Update(ctx context.Context, request updater.UpdateRequest) (updater.UpdateResponse, error) {
	//TODO implement me
	panic("implement me")
}

func NewHelmUpdater() updater.Updater {
	return &helmUpdater{}
}
