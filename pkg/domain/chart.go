package domain

type Chart struct {
	Name    string
	Version string
}
