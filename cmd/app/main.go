package main

import (
	"context"
	"gitlab.com/vsqs/go_sample_app/pkg/helm"
	"gitlab.com/vsqs/go_sample_app/pkg/updater"
	"log"
)

func main() {
	helmUpdater := helm.NewHelmUpdater()
	ctx := context.Background()

	listResponse, err := helmUpdater.List(ctx, updater.ListRequest{})
	if err != nil {
		log.Fatalln("Error :", err)
	}
	updateResponse, err := helmUpdater.Update(ctx, listResponse.ToUpdateRequest())
	if err != nil {
		log.Fatalln("Error :", err)
	}
	log.Println(updateResponse)
}
